# finance.luigi311.com

Blog to post about all things related to finance and FIRE (Financial Independence Retire Early)


## Requirements

-  hugo
-  git

## Usage
Download the remote repo
```bash
    git clone https://gitlab.com/Luigi311/finance.luigi311.com.git
```

Download the submodules
```bash
    git submodule update --init
```

If accessing the site from another device, the baseUrl set to the hostname (e.g. servername.local) and bind set to '0.0.0.0' to allow access from other devices.
```bash
hugo server --baseUrl=$HOSTNAME --bind='0.0.0.0' --disableFastRender
```
