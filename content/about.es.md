+++
title = "Acerca"
description = "Blog para publicar sobre todo lo relacionado con las finanzas y FIRE (Financial Independence Retire Early)"
date = "2021-06-10"
aliases = ["acerca", "contacta-con-nosotros"]
author = "Luigi311"
+++

Hola,

Me llamo Luigi311 y atravesaré mi viaje de FIRE (Financial Independence Retire Early) y compartiré el conocimiento que he acumulado en mi viaje.

En este blog les mostraré en qué estoy invirtiendo, cuál es la asignación y por qué vine a tomar esa decisión.
También discutiré temas financieros que todos deberían conocer y comprender, pero que no tenían a nadie que les informara.