+++
title = "About"
description = "Blog to post about all things related to finance and FIRE (Financial Independence Retire Early)"
date = "2021-06-10"
aliases = ["about-us", "contact"]
author = "Luigi311"
+++

Hello,

I am Luigi311 and I will be going through my journey of FIRE (Financial Independence Retire Early)
and share the knowledge I have accumulated on my journey.

In this blog, we will be discussing what I am investing in and what the allocation of it is.
I will also be discussing financial topics that everyone should know and understand.
