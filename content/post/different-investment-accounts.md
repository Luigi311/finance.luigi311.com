+++
author = "Luigi311"
title = "Different Types of Investment Accounts"
date = "2021-06-10"
description = "List of all the different investment accounts"
featured = true
tags = [
    "investments",
    "accounts",
    "types",
]
categories = [
    "introductions",
]
series = ["Introduction"]
aliases = ["investment-accounts"]
+++

# Different Accounts

Five different accounts allow your money to grow over time. Three of which are retirement accounts, and two are taxable accounts. Retirement accounts are 401K, IRA (Individual Retirement Account), and HSA (Health Savings Account). 401Ks are what most people are familiar with and the most common account. IRAs are like 401Ks but are self-managed instead of being managed by your employer. HSA is like IRAs but needs health-related expenses for the most benefits. The two taxable accounts are brokerage accounts and savings accounts. Brokerage accounts are usually the account when people talk about buying stocks. Savings accounts are checking accounts with more limits and a small interest rate.
 
## Retirement Accounts

Retirement accounts are tax-advantaged, which allows you to save on taxes paid. The downsides are that you can not take money out before reaching the retirement age of 59.5 without paying penalties. There are some exceptions to this rule but are very limited. Retirement accounts also have a yearly contribution limit that depends on tax year and not calendar year, so your time to deposit can vary. Going over the contribution limit will impose fines with the responsibility on you to prevent it. Contributions can also reduce your reported income to the IRS, thus reducing the amount of taxes owed.

### 401K

401Ks are employer-sponsored retirement accounts where the employers deposit the money. They also decide what company manages the 401K and what options you have available. The two most common companies are Vanguard and Fidelity. Employers also have the option of contributing to your account. They can provide a match on your contributions. At the end of the year, they can contribute a certain percent of your salary. Their extra contribution is included in your contract when hired. Benefits should be taken into account when looking for a job as it is extra money. Since it is a percentage of your salary, it can add up to a lot of money. Maximizing the match should be prioritized as that is the only way to guarantee 100% gains on your money. At my job, they provide a 100% match up to 6% of my salary. If 6% of my pay comes out to $300 and I contribute $300, they will put in another $300. Some other employers offer 50% match up to 6% of salary. So when you deposit $300, they will put in another $150. Some employers also provide a year-end contribution to the 401K. My job offers a non-elective 3.5% contribution at year-end. So based on how much my income was, I get an extra 3.5% into my 401K every year. With the 3.5% if you made a total of $40,000 that given year the company will deposit $1,400 into your 401K. The 401K will have the highest yearly contribution limit out of all the retirement accounts. If you are over the age of 50, you have catch-up contributions available. Catch-up contributions increase the contribution limits on your side and your employer. Current contribution limits https://www.irs.gov/retirement-plans/plan-participant-employee/retirement-topics-401k-and-profit-sharing-plan-contribution-limits 
 
### IRA

IRA is like 401K but opened and managed by you at your preferred company and is available if you had taxable income. The downside to it not being an employer-sponsored account is there are no employer contributions. The benefits are that you can open it at any company based on either their customer service or the investment options they offer. IRAs have a lower contribution limit compared to 401Ks but offer more investing options with lower fees. Most people will prefer to contribute to the 401K to the match and then shift to the IRA to the max before maxing 401K. Some other benefits are you can withdraw your Roth IRA contributions without fees or taxes. 

### HSA

HSA offers the most tax advantages so, focus on it before 401K and IRA. HSA needs to be used for health-related expenses for the most benefit but can be used as a traditional IRA once you reach retirement age. HSA accounts are only available to those that are on HDHP health insurance instead of a PPO. The recommendation is if you are healthy and don't have a reoccurring medical issue, get an HDHP plan. It all depends on how risky you are and can afford an accidental medical bill out of pocket. HSA accounts can be both employer-sponsored or not. Money can only be deposited from your paystub if employer-sponsored. HSAs are triple tax-advantaged accounts. Deposited money, gains from investments, and when used for a qualified medical expense are all tax-free as a result. Money is invested in your preferred investment options for your desired growth/risk. You are also allowed to reimburse yourself for any qualified medical expenses that you pay out of pocket. Compensating yourself enables you to keep as much money invested and grow and pay yourself back tax-free. If you get a bill for $1000 and pay for it out of pocket, you can leave that $1000 investing then you can reimburse yourself. After 40 years with six percent growth year to year, $1000 is now worth $10,000. You can now take the $1000 tax-free and pay income tax on the other $9000. The only rule for reimbursing yourself is you need receipts which some HSA accounts allow you to upload a copy. HSAs are one of the only ways to use money tax-free, allowing you to save a lot of money if you save all your receipts.

## Taxable

Taxable are accounts that do not have the benefit of deferring your taxes, unlike retirement accounts. Selling, capital gains/losses are all taken into account when filing taxes. Priority is maxing retirement accounts before taxable, allowing you to save the most in taxes. These would consist of everything from brokerage accounts, savings accounts, and cryptocurrencies.  Taxable accounts do not have contribution limits, so any leftovers go here.

### Brokerage

Brokerage accounts are at investment firms such as Robinhood, Webull,  Wealthfront, and Coinbase. These are usually funded by linking your bank account and transferring money. These accounts are either self-managed or the investment firm based on preference. Having the investment firm handle it will usually come with a management fee, but Wealthfront balances it with their tax lost harvesting. Without tax advantage, you will be paying the income tax when you get paid and then again on your profit. If you sell the stock within the year, you will be taxed on it as income tax. If your income was only $15000 and you sold the stock for $2000 profit, you are only taxed 12% on it for a take-home profit of $1760. Long-term capital gains are when you hold onto a stock for a year before selling it, and those have different rates. If your income is only $15000, you are taxed at 0%, allowing you to take home all $2000 in profit. Current tax rates https://www.irs.gov/taxtopics/tc409

### Savings

Savings accounts are the safest accounts with immediate access but offer the lowest returns. They are usually FDIC insured, so if the market ever crashes and the bank goes bankrupt, the government will return your money to you up to $250,000. Most major banks such as Wells Fargo and Chase offer interest rates of 0.01%, while online-only providers offer up to 3%. The Federal Reserve's interest rates directly affect savings account rates. Recommendations are to only hold onto an emergency fund that will cover up to 6 months worth of expenses in savings accounts. Use cases are being laid off, having an accident, or need a large sum of money. It is imperative to have six months of expenses because accidents can force you to lose your house or rack up massive credit card debt.