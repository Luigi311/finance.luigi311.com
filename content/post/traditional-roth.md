+++
author = "Luigi311"
title = "Traditional and Roth"
date = "2021-06-11"
description = "Differences between a Roth and Traditional and when to use one over the other"
featured = false
tags = [
    "investments",
    "accounts",
    "types",
]
categories = [
    "introductions",
]
series = ["Introduction"]
+++
# Traditional and Roth
401ks and IRAs come in two different flavors, Traditional and Roth, each with their benefits. You can have both types open and contribute to them, but they do share contribution limits. Going over the contribution limit will result in fines.

## Traditional
Traditional is what is most available and most common in 401ks. This is when you deposit pre-tax money, thus allows you to contribute a higher dollar amount. Since the money is pre-taxed, you will pay income taxes when you withdraw from the account. Traditional is best when you are in a high tax bracket and think you will be in a lower tax bracket in retirement. Moving to a state with no or low-income tax is beneficial to reducing the amount you pay.

## Roth 
Roth accounts are less common and not available with every work plan. This allows you to deposit post-tax money, which means all the money in there is already taxed and will grow tax-free. You will not pay any income tax when withdrawing from the account. This is the recommended type if you are currently in a low tax bracket compared to what you will be in the future. When you first start earning income, it is best to use a Roth as you are usually at your lowest income bracket. Over the years, you would then switch over to a Traditional as your income increases. Roth IRA allows you to withdraw any contributions that have been there for over five years without paying any fines on them before you turn 59.5 yrs old. It does not allow you to withdraw any earnings though without paying the fine.

# Company Match
You will still receive the company match whether you are using a Roth or a Traditional account. The matched contributions will always get deposited into the Traditional account.