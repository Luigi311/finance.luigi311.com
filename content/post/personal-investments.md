+++
author = "Luigi311"
title = "Personal Investments"
date = "2021-06-11"
description = "List of all my investments"
featured = true
tags = [
    "investments",
    "personal",
]
categories = [
    "investments"
]
series = ["Investments"]
aliases = ["my-investments"]
+++

This is the breakdown of all the assets I hold and how much of my portfolio it makes up.

Last updated on: Dec 16, 2021

# Total

{{< chart 95 300 >}}
{
    type: 'bar',
    data: {
        labels: ['401k', 'IRA',  'HSA', 'Taxable', 'Crypto', 'Checking', 'Saving' ],
        datasets: [{
            label: 'Percentage by Account Types',
            data: [16.03, 8.68, 4.37, 0.94, 65.93, 1.02, 3.02],
            backgroundColor: [
                'rgba(72, 143, 49, 0.4)',
                'rgba(225, 139, 102, 0.4)',
                'rgba(207, 214, 171, 0.4)',
                'rgba(255, 254, 246, 0.4)',
                'rgba(233, 201, 162, 0.4)',
                'rgba(147, 177, 106, 0.4)',
                'rgba(222, 66, 91, 0.4)'
            ],
            borderColor: [
                'rgba(72, 143, 49, 1)',
                'rgba(225, 139, 102, 1)',
                'rgba(207, 214, 171, 1)',
                'rgba(255, 254, 246, 1)',
                'rgba(233, 201, 162, 1)',
                'rgba(147, 177, 106, 1)',
                'rgba(222, 66, 91, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        maintainAspectRatio: false,
        plugins: {
            legend: false,
            title: {
                display: true,
                text: 'Percentage by Account Type'
            }
        }
    }
}
{{< /chart >}}

{{< chart 95 600 >}}
{
    type: 'bar',
    data: {
        labels: ['VTI', 'VXUS', 'UPRO', 'TMF', 'BB', 'GME', 'COIN', 'BTC', 'ETH', 'ADA', 'ERG', 'XVG', 'UST', 'LUNA', 'APOLLO', 'ANC'],
        datasets: [{
            label: 'Percentage by Account Type',
            data: [18.15, 5.08, 4.09, 2.88, 0.10, 0.56, 0.09, 9.16, 3.41, 0.36, 0.16, 0.10, 4.96, 47.19, 2.31, 1.40],
            backgroundColor: [
                'rgba(72, 143, 49, 0.4)',
                'rgba(225, 139, 102, 0.4)',
                'rgba(207, 214, 171, 0.4)',
                'rgba(255, 254, 246, 0.4)',
                'rgba(233, 201, 162, 0.4)',
                'rgba(147, 177, 106, 0.4)',
                'rgba(222, 66, 91, 0.4)'
            ],
            borderColor: [
                'rgba(72, 143, 49, 1)',
                'rgba(225, 139, 102, 1)',
                'rgba(207, 214, 171, 1)',
                'rgba(255, 254, 246, 1)',
                'rgba(233, 201, 162, 1)',
                'rgba(147, 177, 106, 1)',
                'rgba(222, 66, 91, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        maintainAspectRatio: false,
        indexAxis: 'y',
        plugins: {
            legend: false,
            title: {
                display: true,
                text: 'Percentage by Asset'
            }
        }
    }
}
{{< /chart >}}

# Why I invested in those assets
VTI: This is a market index that tracks the majority of the US stock market like the S&P500 but with more companies. This paired with an international stock market index and bonds should make up the majority of your portfolio to balance out risk and potential gains.

VXUS: This is a market index that tracks the majority of the international stock market. Pair this with VTI to make up the US/International portfolio allocation. I prefer to do a 70/30 split between VTI and VXUS.

UPRO: This is a 3x leveraged S&P500 and is in my portfolio due to utilizing the Hedgefundie strategy. This strategy has not been around for long, this strategy makes up 75% of my IRA account. More info about the strategy here https://www.bogleheads.org/forum/viewtopic.php?f=10&t=288192

TMF: This is a 3x leveraged bond and is also from the Hedgefundie strategy. See above.

BB: This was purchased from the WSB crazy and was a one-off purchase and will not be expanding.

GME: This was purchased from the WSB crazy and was a one-off purchase and will not be expanding.

COIN: Due to how much crypto is booming and most people onboarding into crypto via Coinbase, this seemed like a safe bet as long as they are around. This is a stock pick so it does not make up a big portion of my portfolio.

BTC: The OG in crypto Bitcoin, most of this comes from mining and purchases from long ago. This will only be expanding via mining or cashback rewards from credit cards. This is the crypto that is always talked about and will possibly be around for a long time.

ETH: Another OG in crypto Ethereum, this is the crypto I am most bullish on. A lot of crypto applications are all built upon the Ethereum network and their ETH2.0 which launches in 2021, could potentially solve all the issues it currently has.

ADA: Cardano use case is similar to ETH where lots of applications can be built on top of it. This is my hedge against Eth in case 2.0 does not solve its issues. This is a POS coin with smart contracts coming out this year so ADA and ETH will be battling it out on equal footing.

ETG: Ergo is a POW coin that supports the Cardano network for applications that require POW due to its additional security. The ADA creator also supports the ERG project so the two projects are pretty close to each other and complement one another.

XVG: Verge is a privacy coin I purchased back in 2014 and has remained in my wallet since. This will be sold off at a later date.

UST: Terra is a stable coin backed by algorithms instead of money so is 100% decentralized. During the market crash of 2021, it lost its peg down to $0.90 and then recovered within a day so it seems pretty stable to me. All of this is in the Anchor Protocol on the Luna network earning interest. This will be my stable coin of choice as it is on the Luna network so it has low fees compared to the ETH network that USDC/GUSD/DAI are all based on. UST also has heavy usage in South Korea as many merchants accept it via the Chai app.

LUNA: Luna is the native coin for the Terra network, it is also the token used to keep UST pegged at $1. The more demand there is for UST and the more it deviates from its peg, the more Luna needs to be burned to convert to UST reducing its supply. All of my Luna is also in the Anchor Protocol as collateral for a UST loan.

APOLLO: Apollo token is a governence token for Apollo Protocol on the Terra Network. I was an early adopter and was given most of these tokens as a reward for being a part of the community and during their farming event. I will continue to hold on to these for the forseeable future. Apollos value comes from them taking a fee on their protocol and putting the money into a "warchest" that the dao controls. The warchest will invest the money and will create a sort of floor for the price of the apollo token. Assuming the warchest continues to appreciate in value then the apollo token will continue to grow. Apollo statistics on the warchest and more can be viewed here https://grafana.luigi311.com/d/pA3E5Lt7k/apollo?orgId=2&refresh=1h

ANC: Anc is the governence token for Anchor Protocol on the Terra Network. This is handed out for those that have a anchor loan as an incentive to bring in more loans. This has a buyback mechanism where when anchor is profitable it will use some of the profits to buy back the anc token similar to stock buybacks. Anchor profitability can be viewed here https://grafana.luigi311.com/d/7j96rRI7z/anchor?orgId=2

# By Account

## Retirement Accounts

### Traditional 401k Fidelity

{{< chart 90 200 >}}
{
    type: 'bar',
    data: {
        labels: ['VTI-equivalent', 'VXUS-equivalent'],
        datasets: [{
            label: 'Bar Chart',
            data: [70, 30],
            backgroundColor: [
                'rgba(72, 143, 49, 0.4)',
                'rgba(225, 139, 102, 0.4)',
                'rgba(207, 214, 171, 0.4)',
                'rgba(255, 254, 246, 0.4)',
                'rgba(233, 201, 162, 0.4)',
                'rgba(147, 177, 106, 0.4)',
                'rgba(222, 66, 91, 0.4)'
            ],
            borderColor: [
                'rgba(72, 143, 49, 1)',
                'rgba(225, 139, 102, 1)',
                'rgba(207, 214, 171, 1)',
                'rgba(255, 254, 246, 1)',
                'rgba(233, 201, 162, 1)',
                'rgba(147, 177, 106, 1)',
                'rgba(222, 66, 91, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        maintainAspectRatio: false,
        plugins: {
            legend: false
        }
    }
}
{{< /chart >}}

### Roth IRA M1Finance

https://m1.finance/4rSxG1mejEo0
{{< chart 90 200 >}}
{
    type: 'bar',
    data: {
        labels: ['UPRO', 'TMF', 'VTI', 'VXUS'],
        datasets: [{
            label: 'Bar Chart',
            data: [41.43, 34.10, 17.23, 7.24],
            backgroundColor: [
                'rgba(72, 143, 49, 0.4)',
                'rgba(225, 139, 102, 0.4)',
                'rgba(207, 214, 171, 0.4)',
                'rgba(255, 254, 246, 0.4)',
                'rgba(233, 201, 162, 0.4)',
                'rgba(147, 177, 106, 0.4)',
                'rgba(222, 66, 91, 0.4)'
            ],
            borderColor: [
                'rgba(72, 143, 49, 1)',
                'rgba(225, 139, 102, 1)',
                'rgba(207, 214, 171, 1)',
                'rgba(255, 254, 246, 1)',
                'rgba(233, 201, 162, 1)',
                'rgba(147, 177, 106, 1)',
                'rgba(222, 66, 91, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        maintainAspectRatio: false,
        plugins: {
            legend: false,
        }
    }
}
{{< /chart >}}

### HSA Ameritrade

{{< chart 90 200 >}}
{
    type: 'bar',
    data: {
        labels: ['VTI'],
        datasets: [{
            label: 'Bar Chart',
            data: [100],
            backgroundColor: [
                'rgba(72, 143, 49, 0.4)',
                'rgba(225, 139, 102, 0.4)',
                'rgba(207, 214, 171, 0.4)',
                'rgba(255, 254, 246, 0.4)',
                'rgba(233, 201, 162, 0.4)',
                'rgba(147, 177, 106, 0.4)',
                'rgba(222, 66, 91, 0.4)'
            ],
            borderColor: [
                'rgba(72, 143, 49, 1)',
                'rgba(225, 139, 102, 1)',
                'rgba(207, 214, 171, 1)',
                'rgba(255, 254, 246, 1)',
                'rgba(233, 201, 162, 1)',
                'rgba(147, 177, 106, 1)',
                'rgba(222, 66, 91, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        maintainAspectRatio: false,
        plugins: {
            legend: false
        }
    }
}
{{< /chart >}}

## Taxable Accounts

### Brokerage M1Finance

https://m1.finance/gUkGMFCuU7D4
{{< chart 90 200 >}}
{
    type: 'bar',
    data: {
        labels: ['VTI', 'VXUS'],
        datasets: [{
            label: 'Bar Chart',
            data: [70, 30],
            backgroundColor: [
                'rgba(72, 143, 49, 0.4)',
                'rgba(225, 139, 102, 0.4)',
                'rgba(207, 214, 171, 0.4)',
                'rgba(255, 254, 246, 0.4)',
                'rgba(233, 201, 162, 0.4)',
                'rgba(147, 177, 106, 0.4)',
                'rgba(222, 66, 91, 0.4)'
            ],
            borderColor: [
                'rgba(72, 143, 49, 1)',
                'rgba(225, 139, 102, 1)',
                'rgba(207, 214, 171, 1)',
                'rgba(255, 254, 246, 1)',
                'rgba(233, 201, 162, 1)',
                'rgba(147, 177, 106, 1)',
                'rgba(222, 66, 91, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        maintainAspectRatio: false,
        plugins: {
            legend: false,
        }
    }
}
{{< /chart >}}

### Brokerage Fidelity

{{< chart 90 200 >}}
{
    type: 'bar',
    data: {
        labels: ['GME'],
        datasets: [{
            label: 'Bar Chart',
            data: [100],
            backgroundColor: [
                'rgba(72, 143, 49, 0.4)',
                'rgba(225, 139, 102, 0.4)',
                'rgba(207, 214, 171, 0.4)',
                'rgba(255, 254, 246, 0.4)',
                'rgba(233, 201, 162, 0.4)',
                'rgba(147, 177, 106, 0.4)',
                'rgba(222, 66, 91, 0.4)'
            ],
            borderColor: [
                'rgba(72, 143, 49, 1)',
                'rgba(225, 139, 102, 1)',
                'rgba(207, 214, 171, 1)',
                'rgba(255, 254, 246, 1)',
                'rgba(233, 201, 162, 1)',
                'rgba(147, 177, 106, 1)',
                'rgba(222, 66, 91, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        maintainAspectRatio: false,
        plugins: {
            legend: false,
        }
    }
}
{{< /chart >}}

### Brokerage Webull

{{< chart 90 200 >}}
{
    type: 'bar',
    data: {
        labels: ['GME', 'BB', 'COIN', 'SLV'],
        datasets: [{
            data: [70.37, 13.77, 10.52, 5.34],
            backgroundColor: [
                'rgba(72, 143, 49, 0.4)',
                'rgba(225, 139, 102, 0.4)',
                'rgba(207, 214, 171, 0.4)',
                'rgba(255, 254, 246, 0.4)',
                'rgba(233, 201, 162, 0.4)',
                'rgba(147, 177, 106, 0.4)',
                'rgba(222, 66, 91, 0.4)'
            ],
            borderColor: [
                'rgba(72, 143, 49, 1)',
                'rgba(225, 139, 102, 1)',
                'rgba(207, 214, 171, 1)',
                'rgba(255, 254, 246, 1)',
                'rgba(233, 201, 162, 1)',
                'rgba(147, 177, 106, 1)',
                'rgba(222, 66, 91, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        maintainAspectRatio: false,
        plugins: {
            legend: false,
        }
    }
}
{{< /chart >}}

### Crypto

{{< chart 95 300 >}}
{
    type: 'bar',
    data: {
        labels: ['BTC', 'ETH', 'ADA', 'ERG', 'XVG', 'UST', 'LUNA', 'APOLLO', 'ANC'],
        datasets: [{
            data: [13.26, 4.94, 0.52, 0.23, 0.14, 7.19, 68.33, 3.35, 2.03],
            backgroundColor: [
                'rgba(72, 143, 49, 0.4)',
                'rgba(225, 139, 102, 0.4)',
                'rgba(207, 214, 171, 0.4)',
                'rgba(255, 254, 246, 0.4)',
                'rgba(233, 201, 162, 0.4)',
                'rgba(147, 177, 106, 0.4)',
                'rgba(222, 66, 91, 0.4)'
            ],
            borderColor: [
                'rgba(72, 143, 49, 1)',
                'rgba(225, 139, 102, 1)',
                'rgba(207, 214, 171, 1)',
                'rgba(255, 254, 246, 1)',
                'rgba(233, 201, 162, 1)',
                'rgba(147, 177, 106, 1)',
                'rgba(222, 66, 91, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        maintainAspectRatio: false,
        plugins: {
            legend: false,
        }
    }
}
{{< /chart >}}

### Changelog
- 12/16/2021:
    - Total
        - Added ANC and Apollo holdings
        - Added apollo explanation
    - Crypto
        - Updated holdings

- 09/18/2021
    - Crypto
        - Massive increase in Luna price
        - Sold mTSLA for Apollo Farm Event and then moved to mSPY, will split with mTSLA again in the future
        - Sold MIR as I am no longer farming on Mirror directly and instead on Apollo Farm

- 07/14/2021
    - Split ROTH IRA to 25% VTI/VXUS split and 75% hedgefundie
    - Sold all of my VGX
    - Crypto
        - Converted TSLA to mTSLA in the Mirror Network for LP
        - Sold ANC since Anchor does not have enough collateral to buyback ANC so price is dropping
        - Added MIR to the list of Crypto
