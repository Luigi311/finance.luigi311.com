+++
author = "Luigi311"
title = "Quick Finance Introduction"
date = "2021-07-07"
description = "Introduction to Quick Finance youtube channel"
featured = false
tags = [
    "youtube"
]
categories = [
    "Quick Finance",
]
+++

Welcome to Quick Finance. This channel will focus on providing short and easy-to-understand videos about the financing world. This can vary from how you to invest, how to use certain platforms, what certain accounts are, and so on. This is not a get-rich-quick channel or speculating on when to buy and sell certain stocks. I am not a financial adviser and I am not a financial expert. I am someone who wants to share my knowledge and experience with the community. Due to this, I will not be accepting any sponsorships to avoid any conflicts of interest. The only referral links that I will post in the descriptions are for products that I use daily and enjoy using. Transcripts, my investments, and deeper dives into certain topics are all available over at finance.luigi311.com.